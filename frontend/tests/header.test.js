import header from '../src/components/header'
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML = `
  <div class="header" id="header"></div>
  <div class="menu">
    <span class="menu--language">
        Idiom :
        <select id="lengOption"></select>
    </span>
    <span class="menu--options">
        <a href="http://localhost:8080/#/">Homepage</a>
        <a href="http://localhost:8080/#/contact">Contact</a>
    </span>
  </div>
  <div id="content" class="content"></div>
  <div id="map" class="map"></div></div>
  <div class="footer" id="footer"></div>

  `;

    function fakeDOMLoaded() {
    const fakeEvent = document.createEvent('Event');

    fakeEvent.initEvent('DOMContentLoaded', true, true);
    window.document.dispatchEvent(fakeEvent);
    }

    fakeDOMLoaded();
    new header(
      "#header",
      {address: "C/ Major",cifnif: "",country: "España",facebook: "",icon_logo: "http://localhost:8000/media/icon_logo/ekkoo_logo.jpg",location_lat: "38.000000",location_long: "-0.200000",logo: "http://localhost:8000/media/logo/ekkoo_logo.jpg",mapa_cobertura: "http://localhost:8000/media/mapa_cobertura/ekkoo_logo.jpg",name: "Ekkoo",phone: "",province: "Valencia",textos: [ {}, {}, {} ],twitter: "https://twitter.com/josep_soler_pla",zipcode: "46870"},
      {contact: "Contact",homepage: "Homepage",list: "List",message: "Message",name: "Name",phone: "Phone",price: "Price",send: "Send",tariff: "Tariff",your: "Your"}
      )

});
test('Template has been generated', () => {
  expect($('#header').children.length).toBeGreaterThan(0);   
});
test('Data has been inserted, checking twitter href on a', () => {
  expect(document.getElementById("titleEnterprise").innerHTML).toBe("Ekkoo");   
});