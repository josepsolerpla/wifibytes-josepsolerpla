const index=require('../src/index.js');
const $ = require('jquery');


beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML = `
    <body>
      <div class="header" id="header"></div>
      <div class="menu">
        <span class="menu--language">
            Idiom :
            <select id="lengOption"></select>
        </span>
        <span class="menu--options">
            <a href="http://localhost:8080/#/">Homepage</a>
            <a href="http://localhost:8080/#/contact">Contact</a>
        </span>
      </div>
      <div id="content" class="content">
      </div>
      <div class="footer" id="footer"></div>
    </body>
    `;
  function fakeDOMLoaded() {
    const fakeEvent = document.createEvent('Event');
    fakeEvent.initEvent('DOMContentLoaded', true, true);
    window.document.dispatchEvent(fakeEvent);
  }
  fakeDOMLoaded();
});

test('HTDOM is not empty', () => {
  expect(document.body.children.length).toBeGreaterThan(0);   
});
test('Index load Header', () => {
  expect($('#header').children.length).toBeGreaterThan(1);   
});
test('Index load Homepage', () => {
  expect($('#homepage').children.length).toBeGreaterThan(1);   
});
test('Index load Footer', () => {
  expect($('#footer').children.length).toBeGreaterThan(1);   
});