import footer from '../src/components/footer';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
    document.body.innerHTML = `
      <div class="header" id="header"></div>

      <div id="content" class="content"></div>

      <div class="footer" id="footer"></div>
  `;

    function fakeDOMLoaded() {
    const fakeEvent = document.createEvent('Event');

    fakeEvent.initEvent('DOMContentLoaded', true, true);
    window.document.dispatchEvent(fakeEvent);
    }
    let Utils = {
      deleteCookie : jest.fn(),
      get : jest.fn(),
      getCookie : jest.fn(),
      setCookie : jest.fn(),
    }
    fakeDOMLoaded();
    new footer(
      "#footer",
      {"name":"Ekkoo","cifnif":"","phone":"","logo":"http://localhost:8000/media/logo/ekkoo_logo.jpg","icon_logo":"http://localhost:8000/media/icon_logo/ekkoo_logo.jpg","mapa_cobertura":"http://localhost:8000/media/mapa_cobertura/ekkoo_logo.jpg","address":"C/ Major","city":"Ontinyent","province":"Valencia","country":"España","zipcode":"46870","location_lat":"38.000000","location_long":"-0.200000","facebook":null,"twitter":"https://twitter.com/","textos":[{"key":"jumbotron 1","content":"<p>This is a jumbotron</p>","image":"http://localhost:8000/media/info_empresa_image/ekkoo_logo.jpg","lang":"en"},{"key":"jumbotron 2","content":"<p>another one</p>","image":null,"lang":"en"}]},
      {en : {contact: "Contact",homepage: "Homepage",list: "List",message: "Message",name: "Name",phone: "Phone",price: "Price",send: "Send",tariff: "Tariff",your: "Your",__proto__: Object},es : {contact: "Contactanos",homepage: "Inicio",list: "Catalogo",message: "Mensaje",name: "Nombre",phone: "Telefono",price: "Precio",send: "Enviar",tariff: "Tarifas",your: "Tu",}},
      Utils
    )

    new footer(
      "#footer",
      {"name":"Ekkoo","cifnif":"","phone":"","logo":"http://localhost:8000/media/logo/ekkoo_logo.jpg","icon_logo":"http://localhost:8000/media/icon_logo/ekkoo_logo.jpg","mapa_cobertura":"http://localhost:8000/media/mapa_cobertura/ekkoo_logo.jpg","address":"C/ Major","city":"Ontinyent","province":"Valencia","country":"España","zipcode":"46870","location_lat":"38.000000","location_long":"-0.200000","facebook":null,"twitter":"https://twitter.com/","textos":[{"key":"jumbotron 1","content":"<p>This is a jumbotron</p>","image":"http://localhost:8000/media/info_empresa_image/ekkoo_logo.jpg","lang":"en"},{"key":"jumbotron 2","content":"<p>another one</p>","image":null,"lang":"en"}]}
      )

});
test('Template has been generated', () => {
  expect($('#footer').children.length).toBeGreaterThan(0);   
});
test('Data has been inserted, checking twitter href on a', () => {
  expect(document.querySelectorAll("[href='https://twitter.com/']").length).toBe(1);   
});