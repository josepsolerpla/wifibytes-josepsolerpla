import * as Utils from '../src/services/utils'

const mockXHR = {
  open: jest.fn(),
  onload: jest.fn(),
  onerror: jest.fn(),
  send: jest.fn(),
  readyState: 4,
  status: 200,
  statusText: JSON.stringify(
      [
          { title: 'test post' },
          { tile: 'second test post' }
      ]
  ),
  onreadystatechange : function() {
    if (this.readyState === 4) {
            const resp = JSON.parse(this.statusText);
            if (resp.error) {
                return(resp.error);
            } else {
                return(resp);
            }
        }
  }
};

const oldXMLHttpRequest = window.XMLHttpRequest;
window.XMLHttpRequest = jest.fn(() => mockXHR);

beforeEach(()=> {
});

test('Get function', () => {
  const reqPromise = Utils.get();
  mockXHR.onreadystatechange();
  reqPromise.then((posts) => {
      expect(posts.length).toBe(2);
      expect(posts[0].title).toBe('test post');
      expect(posts[1].title).toBe('second test post');
      done();
  });
});

test('setCookie function', () => {
    Utils.setCookie("test","test")

    expect(Utils.getCookie("test")).toBe("test")
});

test('getCookie function', () => {
    Utils.setCookie("test","test")

    expect(Utils.getCookie("test")).toBe("test")
});
test('getCookie UNDEFINED function', () => {
    Utils.setCookie("test","test")

    expect(Utils.getCookie("random")).toBe(undefined)
});
test('deleteCookie function', () => {
    Utils.setCookie("test","test")
    expect(Utils.getCookie("test")).toBe("test")
    Utils.deleteCookie("test")
    expect(Utils.getCookie("test")).toBe(undefined)
});
