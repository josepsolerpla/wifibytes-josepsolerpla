import Contact from '../src/components/contact';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
    document.body.innerHTML = `
        <div class="header" id="header"></div>

        <div id="content" class="content"></div>

        <div class="footer" id="footer"></div>
    `;

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');

  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}

fakeDOMLoaded();

let validateData = jest.fn((formInfo)=>{
        let res = true;
        if(formInfo.name == ""){
            $("#errName")[0].innerHTML = "You have to introduce a name";
            res = false
        }else{
            $("#errName")[0].innerHTML = "";
        }
        if(formInfo.email == ""){
            $("#errEmail")[0].innerHTML = "You have to introduce a email";
            res = false
        }else{
            $("#errEmail")[0].innerHTML = "";
        }
        if(formInfo.phone == ""){
            $("#errPhone")[0].innerHTML = "You have to introduce a phone";
            res = false
        }else{
            $("#errPhone")[0].innerHTML = "";
        }
        if(formInfo.message == ""){
            $("#errMessage")[0].innerHTML = "You have to introduce a message";
            res = false
        }else{
            $("#errMessage")[0].innerHTML = "";
        }
  return res;
})

let contactService = {
  submitForm :jest.fn((formInfo)=>{validateData(formInfo)})
}

let getLangs = {contact: "Contact",homepage: "Homepage",list: "List",message: "Message",name: "Name",phone: "Phone",price: "Price",send: "Send",tariff: "Tariff",your: "Your"}

new Contact(contactService,getLangs)

});
test('Template has been generated', () => {
  expect($("#content").children.length).toBeGreaterThan(0);   
});

test('Test for Submit Form, with errors', () => {
  $("#submitForm").click();

  expect($("#errName")[0].innerHTML).toBe('You have to introduce a name');  
  expect($("#errPhone")[0].innerHTML).toBe('You have to introduce a phone');   
  expect($("#errEmail")[0].innerHTML).toBe('You have to introduce a email');   
  expect($("#errMessage")[0].innerHTML).toBe('You have to introduce a message');   
});

test('Test for Submit Form, no errors', () => {
  $("#name")[0].value = "name"
  $("#phone")[0].value = "phone"
  $("#email")[0].value = "email"
  $("#message")[0].value = "message"
  
  $("#submitForm").click();

  expect($("#errName")[0].innerHTML).toBe('');  
  expect($("#errPhone")[0].innerHTML).toBe('');   
  expect($("#errEmail")[0].innerHTML).toBe('');   
  expect($("#errMessage")[0].innerHTML).toBe(''); 
});