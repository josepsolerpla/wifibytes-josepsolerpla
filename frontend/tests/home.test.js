import home from '../src/components/home'
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML = `
    <div class="header" id="header"></div>

    <div id="content" class="content"></div>

    <div class="footer" id="footer"></div>
  `;

    function fakeDOMLoaded() {
    const fakeEvent = document.createEvent('Event');

    fakeEvent.initEvent('DOMContentLoaded', true, true);
    window.document.dispatchEvent(fakeEvent);
    }

    fakeDOMLoaded();

    new home(
        {"count":1,"next":null,"previous":null,"results":[{"codtarifa":1,"nombretarifa":"Ekko Gold","slug":"ekko-gold","pretitulo":"Ekko Gold","logo":"http://localhost:8000/media/Logo/ekkoo_logo.jpg","precio":80.0,"activo":true,"destacado":true,"color":{"id":1,"titulo":"golden","hexadecimal":"#ffd700"},"subtarifas":[{"subtarifa_id":1,"subtarifa_datos_internet":35.0,"subtarifa_cent_minuto":null,"subtarifa_est_llamada":null,"subtarifa_precio_sms":null,"subtarifa_minutos_gratis":null,"subtarifa_minutos_ilimitados":false,"subtarifa_velocidad_conexion_subida":null,"subtarifa_velocidad_conexion_bajada":null,"subtarifa_num_canales":null,"subtarifa_siglas_omv":"ESTE","subtarifa_omv":null,"tipo_tarifa":1,"subtarifa_tarifa":{"codtarifa":1,"nombretarifa":"Ekko Gold","slug":"ekko-gold","pretitulo":"Ekko Gold","pretitulo_va":"","logo":"media/Logo/ekkoo_logo.jpg","precio":80.0,"activo":true,"destacado":true,"created_at":1541693685,"updated_at":1541693685,"color":1}},{"subtarifa_id":2,"subtarifa_datos_internet":23.0,"subtarifa_cent_minuto":null,"subtarifa_est_llamada":null,"subtarifa_precio_sms":null,"subtarifa_minutos_gratis":null,"subtarifa_minutos_ilimitados":false,"subtarifa_velocidad_conexion_subida":null,"subtarifa_velocidad_conexion_bajada":null,"subtarifa_num_canales":null,"subtarifa_siglas_omv":"ELATLRE","subtarifa_omv":null,"tipo_tarifa":1,"subtarifa_tarifa":{"codtarifa":1,"nombretarifa":"Ekko Gold","slug":"ekko-gold","pretitulo":"Ekko Gold","pretitulo_va":"","logo":"media/Logo/ekkoo_logo.jpg","precio":80.0,"activo":true,"destacado":true,"created_at":1541693685,"updated_at":1541693685,"color":1}}]}]},
        {contact: "Contact",homepage: "Homepage",list: "List",message: "Message",name: "Name",phone: "Phone",price: "Price",send: "Send",tariff: "Tariff",your: "Your"}
    )
});
test('Template has been generated', () => {
  expect($('#content').children.length).toBeGreaterThan(0);   
});
test('Data has been inserted, checking how many tarifas has seended (1)', () => {
  expect(document.querySelectorAll(".tarifa").length).toBe(1);   
});