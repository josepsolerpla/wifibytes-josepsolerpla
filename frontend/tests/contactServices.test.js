import * as contactServices from '../src/services/contact.services';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
    document.body.innerHTML = `
        <div class="header" id="header"></div>
        <div id="content" class="content">
            <form id="formContaºct">
                <input id="name" type="renderText.text">
                <span id="errName"></span>
                <input id="phone" type="renderText.text">
                <span id="errPhone"></span>
                <input id="email" type="email">
                <span id="errEmail"></span>
                <textarea id="message"></textarea>
                <span id="errMessage"></span>
            </form>
        </div>
        <div class="footer" id="footer"></div>
    `;

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');

  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}

fakeDOMLoaded();

});
test('Template has been generated', () => {
  expect($("#content").children.length).toBeGreaterThan(0);   
});

test('Test for Submit Form, with errors', () => {
    contactServices.submitForm({name:null,phone:null,email:null,message:null})

    expect($("#errName")[0].innerHTML).toBe('You have to introduce a name');  
    expect($("#errPhone")[0].innerHTML).toBe('You have to introduce a phone');   
    expect($("#errEmail")[0].innerHTML).toBe('You have to introduce a email');   
    expect($("#errMessage")[0].innerHTML).toBe('You have to introduce a message');   
});

test('Test for Submit Form, no errors', () => {
    contactServices.submitForm({name:"test",phone:"test",email:"test",message:"test"})

    expect($("#errName")[0].innerHTML).toBe('');  
    expect($("#errPhone")[0].innerHTML).toBe('');   
    expect($("#errEmail")[0].innerHTML).toBe('');   
    expect($("#errMessage")[0].innerHTML).toBe(''); 
});