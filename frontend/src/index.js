/**
 * Utils
 */
import {Router} from './services/router.service'
import jumbotron from './components/jumbotron';
import * as Utils from './services/utils'
import {initMap} from './services/mapGoolge.services'
import {getLangs} from './lib/lenguage'
import * as contactService from './services/contact.services'
import * as jumbotronService from './services/jumbotron.services'
/**
 * $components
 */
import tarifas from './components/tarifas'
import header from './components/header';
import home from './components/home';
import contact from './components/contact';
import footer from './components/footer';
import catalogo from './components/catalogo'
/**
 * Check Cookies and if not Set Default English
 */
let Lenguage = Utils.getCookie("lang");
if(!Lenguage){
    Utils.setCookie("lang","en")
    Lenguage = "en";
}
/**
 * Router
 * Declare all routes on app
 */
Router
.add(/contact/, function() {
    Utils.get('/datos_empresa/').then(function(datos_empresa){
        new contact(contactService,getLangs[Lenguage])
        initMap({lat:parseInt(datos_empresa.location_lat),lng:parseInt(datos_empresa.location_long)}) //Initialize the MapGoogle
    })
}).listen()
.add(/products\/(.*)\/edit\/(.*)/, function() {
    console.log('products', arguments);
})
.add(/tarifas/, function() {
    Utils.get('/tarifa/').then(function(tarifaData){
        new tarifas(tarifaData,getLangs[Lenguage]);
    })
})
.add(/catalogo/, function() {
    Utils.get('/articulo/').then(function(articuloData){
    Utils.get('/filtros/').then(function(filtros){
        new catalogo(filtros,articuloData,getLangs[Lenguage],Utils);
    })})
})
.add(function() {
    Utils.get('/tarifa/').then(function(tarifa){
    Utils.get('/datos_empresa/').then(function(datos_empresa){
            new home(tarifa,getLangs[Lenguage])
            new jumbotron(jumbotronService,datos_empresa.textos.filter((obj)=>{return obj.key.includes("jumbotron")}))
        })
    })
});
/**
 * Load Header and Footer with data
 * @param datos_empresa Get from backend
 */
Utils.get('/datos_empresa/').then(function(datos_empresa){
    new header("#header",datos_empresa,getLangs[Lenguage])
    new footer("#footer",datos_empresa,getLangs,Utils)
}).catch((err)=>{
    console.log("error",err)
})
/**
 * Load Homepage
 */
document.addEventListener("DOMContentLoaded", function(event) {
    Router.navigate("/home");
})