import {Settings} from '../settings';
/*
 $cacheTemplates

 Function used for Get Petition to the BackEnd
*/
let CACHE_TEMPLATES = new Map();
/** Ajax call 
 * @param url endpoint
 * @param callback function to be executed when we get a real answer from server. This callback always get the real response as parameter
*/
function get(url) {
    return new Promise(function(resolve, reject) {
      if (CACHE_TEMPLATES.has(url)) { // Check the ' Cache ' and if it already has on it return that information 
        resolve(CACHE_TEMPLATES.get(url));
      }else{ //If not do a normal XMLHttpRequest
        var req = new XMLHttpRequest();
        req.open('GET', Settings.baseURL+url);
    
        req.onload = function() {
          if (req.readyState===4 && req.status == 200) {
            resolve(JSON.parse(req.response));
          }
          else {
            reject(Error(req.statusText));
          }
        };
        req.onerror = function() {
          reject(Error("Network Error"));
        };
        req.send();
      }
    });
}
/**
 * setCookie
 * @param name name of the cookie to set
 * @param value value of the cookie to set
 */
function setCookie(name,value){
  document.cookie = name+"="+value
}
/**
 * getCookie
 * @param name name of the cookie to get 
 */
function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}
/**
 * deleteCookie
 * @param name name of the cookie to delete 
 */
function deleteCookie(name) {
  document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
/**
 * delegate
 * "on" on JQuary
 */
function delegate(el, evt, sel, handler) {
  el.addEventListener(evt, function(event) {
      var t = event.target;
      while (t && t !== this) {
          if (t.matches(sel)) {
              handler.call(t, event);
          }
          t = t.parentNode;
      }
  });
}
export {get,setCookie,getCookie,deleteCookie,delegate};