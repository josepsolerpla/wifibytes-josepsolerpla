let slideIndex = 1; //Asign the first page

function showSlides(n) {
    var slides = document.querySelectorAll(".mySlides");
    if (n > slides.length) {slideIndex = 1}    
    if (n < 1) {slideIndex = slides.length}
    for (var i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";  
    }
    slides[slideIndex-1].style.display = "flex";  
}
function plusSlides(n) {
    showSlides(slideIndex += n);
}
export {plusSlides,showSlides}