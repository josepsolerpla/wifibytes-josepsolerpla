/** 
 * initMap
 * Function to initialize Map
 */
function initMap(arrayMap) {
    new google.maps.Map(document.getElementById('map'), {
        center: {lat: (arrayMap.lat ? arrayMap.lat : 0), lng: (arrayMap.lng ? arrayMap.lng : 0)},
        zoom: (arrayMap.lat && arrayMap.lng ? 8: 1)
    });
}

export {initMap}