/** 
 * submitFrom
 * @param formInfo
 */
function submitForm(formInfo){
    let res = validateData(formInfo) //Validate Data using $validateData
    if(res){ //Depending on the Validate Send or Show Error
       // console.log({data:formInfo})
    }else{
        //console.log({err:"error"})
    }
}
/** 
 * validateData
 * @param formInfo
 */
function validateData(formInfo){
    let res = true;
    if(!formInfo.name){
        document.getElementById("errName").innerHTML = "You have to introduce a name";
        document.getElementById("name").style.color = "red"; 
        res = false
    }else{
        document.getElementById("errName").innerHTML = "";
        document.getElementById("name").style.color = "black"; 
    }
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!formInfo.email){
        document.getElementById("errEmail").innerHTML = "You have to introduce a email";
        document.getElementById("email").style.color = "red"; 
        res = false
    }else{
        document.getElementById("errEmail").innerHTML = "";
        document.getElementById("email").style.color = "black"; 
    }
    if(!formInfo.phone){
        document.getElementById("errPhone").innerHTML = "You have to introduce a phone";
        document.getElementById("phone").style.color = "red"; 
        res = false
    }else{
        document.getElementById("errPhone").innerHTML = "";
        document.getElementById("phone").style.color = "black"; 
    }
    if(!formInfo.message){
        document.getElementById("errMessage").innerHTML = "You have to introduce a message";
        document.getElementById("message").style.color = "red"; 
        res = false
    }else{
        document.getElementById("errMessage").innerHTML = "";
        document.getElementById("message").style.color = "black"; 
    }

    return res;
}
export {submitForm}