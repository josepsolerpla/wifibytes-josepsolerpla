import app from './appCtrl'

export default class Catalogo extends app{
    constructor(filtros,articuloData,text){
        /**
         * Data to send to app Ctrl
         */
        let renderText = {
            filtros : filtros,
            articuloData : articuloData.results,
            textPage : text
        }
        let render = ()=>{
            return `
            <div class="catalog">
                <div class="catalogo__blockfilter">
                <form>
                    <select id="pantalla">
                        <option>Select One</option>
                        ${renderText.filtros.pantalla.map((obj)=>{
                            return `<option value="${obj.id}">${obj.num_pantalla}</option>`
                        })}
                    </select>
                    <select id="procesador">
                        <option>Select One</option>
                        ${renderText.filtros.procesador.map((obj)=>{
                            return `<option value="${obj.id}">${obj.num_procesador}</option>`
                        })}
                    </select>
                    <select id="ram">
                        <option>Select One</option>
                        ${renderText.filtros.ram.map((obj)=>{
                            return `<option value="${obj.id}">${obj.num_ram}</option>`
                        })}
                    </select>
                    <select id="marca">
                        <option>Select One</option>
                        ${renderText.filtros.marca.map((obj)=>{
                            return `<option value="${obj.id}">${obj.Marca}</option>`
                        })}
                    </select>
                    <select id="camara">
                        <option>Select One</option>
                        ${renderText.filtros.camara.map((obj)=>{
                            return `<option value="${obj.id}">${obj.num_camara}</option>`
                        })}
                    </select>
                    <input type="submit" id="filterSearch" value="filter">
                </form>
                </div>
                <div class="catalogo__blocklist">
                    ${renderText.articuloData.length == 0 ? `Not found`: 
                        renderText.articuloData.map((articulo)=>{return `
                        <div class="article">
                                <img src="${articulo.imagen}" style="width:50px">
                                <h1>${articulo.descripcion}</h1>
                                <h3>${renderText.textPage.price} : ${articulo.pvp}</h3>
                                <p>${renderText.textPage.descripcion_breve}</p>
                            </div>`
                        }).join('')
                    }
                <div>
            <div class="">`
        }
        /**
         * Supper
         */
        super(render,renderText)
        /**
         * Functions of the controller
         */
        this.Utils.delegate(document, "click", "#filterSearch", ()=>{
            renderText.articuloData = articuloData.results
            for(var item in renderText.filtros){
                if(document.querySelector("#"+item).value != "Select One"){
                    renderText.articuloData = articuloData.results.filter((obj)=>{return obj[item] == document.querySelector("#"+item).value})
                }
            }
            this.updateRender(render)
        });
    }
}