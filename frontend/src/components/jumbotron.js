import app from './appCtrl'

export default class Jumbotron extends app{
    constructor(JumbotronService,data){
    /**
     * Data to send to app Ctrl
    */
        let renderText = {
            data : data //Data filtered by Lang
        }
        let render = (renderText)=>{
            return `
            <div class="jumbotron" id="jumbotron">
                <div class="slideshow-container">
                        ${renderText.data.map((obj)=>{return`
                            <div class="mySlides fade" style="background-image:url('${obj.image ? obj.image : 'http://via.placeholder.com/800x200'}')">
                                ${obj.content} 
                            </div>
                        `}).join('')}
                    <a class="prev ${renderText.data.length <= 1 ? `hidden`:``}">&#10094;</a>
                    <a class="next ${renderText.data.length <= 1 ? `hidden`:``}">&#10095;</a>
                </div>
            </div>`
        }
    /**
     * Supper
     */
        super(render,renderText,'.jumbotron')
    /**
     * Functions of the controller
    */
        JumbotronService.showSlides(1)   //Set the first page   
        /**
         * When a button is pressed the page will change
         */
        document.querySelector('.prev').addEventListener('click',function(){JumbotronService.plusSlides(-1)})
        document.querySelector('.next').addEventListener('click',function(){JumbotronService.plusSlides(1)})
        /**
         * Set a interval timer to do this function each time
         */
        setInterval(()=>{
            if(document.querySelector('.jumbotron') && renderText.data.length > 1){
                JumbotronService.plusSlides(1)
            }
        },6000)
    }
}