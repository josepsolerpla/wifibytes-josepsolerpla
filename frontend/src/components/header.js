import app from './appCtrl'

export default class Header extends app{
    constructor(elementPrint,datos_empresa,textPage) {
    /**
     * Data to send to app Ctrl
     */
        let renderText = {
            textPage : textPage,
            datos_empresa : datos_empresa
        }
        let render = ()=>{return  `
            <h1 class="title" id="titleEnterprise">${renderText.datos_empresa.name}</h1>
            <img class="image" id="imageEnterprise" src="${renderText.datos_empresa.logo}">
            <nav>
                <span class="menu--options">
                    <a href="http://localhost:8080/#/">${renderText.textPage.homepage}</a>
                    <a href="http://localhost:8080/#/contact">${renderText.textPage.contact}</a>
                    <a href="http://localhost:8080/#/tarifas">${renderText.textPage.tariff}</a>
                    <a href="http://localhost:8080/#/catalogo">${renderText.textPage.list}</a>
                </span>
            </nav>`
        }
    /**
     * Supper
     */
        super(render,renderText,elementPrint)
    /**
     * Functions of the controller
    */
    }
}