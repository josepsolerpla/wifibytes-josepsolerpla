import app from './appCtrl'

export default class Contact extends app{
    constructor(contactService,textPage){
    /**
     * Data to send to app Ctrl
     */
        let renderText = {
            textPage
        }
        let render = (renderText)=>{return `
                <div class='contact' id='contact'>
                        <form id="formContaºct">
                            <span>
                                <label>${(renderText.textPage.your+" "+renderText.textPage.name)}:</label>
                                <input id="name" type="text" required>
                                <span class="error" id="errName"></span>
                            </span><br>
                            <span>
                                <label>${(renderText.textPage.your+" "+renderText.textPage.phone)}:</label>
                                <input id="phone" type="phone" required minlength="9">
                                <span class="error" id="errPhone"></span>
                            </span><br>
                            <span>
                                <label>${renderText.textPage.your} Email:</label>
                                <input id="email" type="email" required>
                                <span class="error" id="errEmail"></span>
                            </span><br>
                            <span>
                                <label>${renderText.textPage.message} :</label>
                                <textarea id="message"></textarea>
                                <span class="error" id="errMessage"></span>
                            </span>
                            <input type="button" id="submitForm" value="${renderText.textPage.send}">
                        </form>
                    </div>
                    <div id="map" class="map"></div>
                    `
        }
    /**
     * Supper
     */
        super(render,renderText)
    /**
     * Functions of the controller
    */
        /**
         * Subit && Validate the Form When subitForm button is clicked
         * @param formInfo comes from the formulary of DOMHTML
         */
        document.getElementById("submitForm").addEventListener("click",function(){
            let formInfo = { //Get all values from Dom
                name: document.getElementById('name').value,
                phone: document.getElementById('phone').value,
                email: document.getElementById('email').value,
                message: document.getElementById('message').value,
            }
            contactService.submitForm(formInfo) //Submit function -> Validate Info -> Show Errors -> consolelog
        })
    }
}