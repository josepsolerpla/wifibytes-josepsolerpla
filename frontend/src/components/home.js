import app from './appCtrl'

export default class Home extends app{
    constructor(tarifa,textPage){
    /**
     * Data to send to app Ctrl
     */
        let renderText = {
            textPage : textPage,
            tarifas: tarifa.results.filter((obj)=>{return obj.destacado == true}) //Parse Data + Data filtered to get just the destacated
        }
        let render = ()=>{
            return  `
            <div class='homepage' id='homepage'>
                <section class="jumbotron"></section>
                <section class="homepage__blocktarifa" id='homepage--blocktarifa'>
                        ${renderText.tarifas.map((tarifa)=>{return `
                            <div class="tarifa">
                                <img src="${tarifa.logo}" style="width:50px">
                                <h1>${tarifa.nombretarifa}</h1>
                                <h3>${renderText.textPage.price} : ${tarifa.precio}</h3>
                                ${tarifa.subtarifas.map((subtarifas)=>{return `
                                    <p>${subtarifas.subtarifa_siglas_omv}</p>`
                                }).join('')}
                            </div>`
                        }).join('')}
                </section>
            </div>`
        } 
    /**
     * Supper
     */
        super(render,renderText)
    /**
     * Functions of the controller
    */
    }
}