import app from './appCtrl'

export default class Tarifas extends app{
    constructor(tarifaData,text){
        console.log(tarifaData.results)
        /**
         * Data to send to app Ctrl
         */
        let renderText = {
            tarifaData : tarifaData.results,
            textPage : text
        }
        let render = ()=>{
            return `
            <div class="tarifa__block">
                ${renderText.tarifaData.map((tarifa)=>{return `
                <div class="tarifa">
                        <img src="${tarifa.logo}" style="width:50px">
                        <h1>${tarifa.nombretarifa}</h1>
                        <h3>${renderText.textPage.price} : ${tarifa.precio}</h3>
                        <h3>Subtarifa : </h3>
                        <ul>
                            ${tarifa.subtarifas.map((subtarifas)=>{return `
                                <li>${subtarifas.subtarifa_siglas_omv}</li>`
                            }).join('')}
                        </ul>
                </div>`
            }).join('')}
            </div>`
        }
        /**
         * Supper
         */
        super(render,renderText)
        /**
         * Functions of the controller
         */
    }
}