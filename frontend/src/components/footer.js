import app from './appCtrl'

export default class Footer extends app{
    constructor(elementPrint,datos_empresa,language,Utils) {
        let langDisp =[] ; for(let lang in language){langDisp.push(lang)} //Take all languages available    
    /**
    * Data to send to app Ctrl
    */
        let renderText = {
            datos_empresa : datos_empresa,
            language : langDisp,
            Utils : Utils
        }
        let render = ()=>{
            return  `
            <div class="footerLinks__block">
                ${renderText.datos_empresa.textos ? renderText.datos_empresa.textos.filter((obj)=>{return obj.key.match(/^((?!jumbotron).)*$/)}).map((textos,index)=>{return`
                    ${index == (renderText.datos_empresa.textos.length -1) ?
                        `<a href="http://localhost:8080/#/${textos.key.toLowerCase().replace(' ','%')}">${textos.key}</a>`
                        :
                        `<a href="http://localhost:8080/#/${textos.key.toLowerCase().replace(' ','%')}">${textos.key}</a> |`
                    }`
                }).join('') : ''}
            </div>
            <div class="footerSocial__block">
                ${renderText.datos_empresa.facebook ? 
                    `<a href="${renderText.datos_empresa.facebook}">Facebook</a>`
                    :
                    ``
                }
                ${renderText.datos_empresa.twitter ?
                    `<a href="${renderText.datos_empresa.twitter}">twitter</a>`
                    :
                    ``
                }
            </div>
            <div class="footerLanguage__block">
                <span>Idiom :</span>
                <select id="lengOption">${renderText.language.map((obj)=>{
                    return obj == renderText.Utils.getCookie("lang") ? `<option selected="true">${obj}</option>` : `<option>${obj}</option>` 
                }).join('')}</select>
            </div>
        `
        }
    /**
     * Supper
     */
        super(render,renderText,elementPrint)
    /**
     * Functions of the controller
    */
        /**
         * Change Language
         * @param document.querySelector("#lengOption").value from DOMHTML
         */
        document.querySelector("#lengOption").addEventListener("change",function(res){
            Utils.setCookie("lang",document.querySelector("#lengOption").value)
            location.reload();
        })
    }
}