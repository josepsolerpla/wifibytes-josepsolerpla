import * as Utils from '../services/utils'

class appCtrl{
    constructor(render,renderText,elementPrint = "#content"){
        this.selectedTarget
        this.elementPrint = elementPrint;
        this.Utils = Utils;
        try{
            this.selectedTarget=document.querySelector(this.elementPrint);
            if(this.selectedTarget)this.selectedTarget.innerHTML = render(renderText);
            else throw("Error. Selected output target for component "+this.constructor.name+" doesn't exist");
        }catch(e){
            if (this.selectedTarget)this.selectedTarget.innerHTML="Problems rendering "+this.constructor.name+" -> "+e;
            throw e;
        }
    }

    updateRender(newRender){
        try{
            this.selectedTarget=document.querySelector(this.elementPrint);
            if(this.selectedTarget)this.selectedTarget.innerHTML = newRender();
            else throw("Error. Selected output target for component "+this.constructor.name+" doesn't exist");
        }catch(e){
            if (this.selectedTarget)this.selectedTarget.innerHTML="Problems rendering "+this.constructor.name+" -> "+e;
            throw e;
        }
    }
}
export default appCtrl